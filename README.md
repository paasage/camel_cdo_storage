CAMEL storage example
=======================

Overview
=======================

Reads a .camel file from a String or a File and stores it in CDO.

Configure
=======================

Configure CDO Client.

Install
=======================
```
mvn clean install
```

Usage
=======================
```
See App.java for a usage example.
```

License
========================
Mozilla Public License Version 2.0

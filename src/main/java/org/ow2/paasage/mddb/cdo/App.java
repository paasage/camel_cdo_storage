package org.ow2.paasage.mddb.cdo;

import eu.paasage.camel.CamelModel;
import org.eclipse.xtext.util.Files;

import java.io.File;

/**
 * Hello world!
 */
public class App {
    private static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(App.class);


    public static void main(String[] args) {
        //Usage
        CamelModel camelModelFromFile = CamelXtextLoader.load(new File("test.camel"));
        CamelModel camelModelFromString = CamelXtextLoader.load(Files.readFileIntoString("test.camel"));

        ModelStorage.of("cdoUser", "cdoPass").storeToCdo(camelModelFromFile, "cdoResourceName");

    }
}

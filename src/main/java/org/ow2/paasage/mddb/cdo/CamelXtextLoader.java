package org.ow2.paasage.mddb.cdo;

import com.google.common.base.Charsets;
import com.google.common.io.Files;
import com.google.inject.Injector;
import eu.paasage.camel.CamelModel;
import eu.paasage.camel.dsl.CamelDslStandaloneSetup;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.xtext.resource.XtextResource;
import org.eclipse.xtext.resource.XtextResourceSet;

import java.io.File;
import java.io.IOException;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by daniel on 13.06.16.
 */
public class CamelXtextLoader {

    private CamelXtextLoader() {
        throw new AssertionError("static class");
    }

    private static CamelModel doLoad(String filePath) {
        Injector injector = (new CamelDslStandaloneSetup()).createInjectorAndDoEMFRegistration();
        XtextResourceSet rs = injector.getInstance(XtextResourceSet.class);
        PackageRegistration.registerCamel(rs.getPackageRegistry());
        rs.addLoadOption(XtextResource.OPTION_RESOLVE_ALL, Boolean.TRUE);
        Resource res = rs.getResource(URI.createFileURI(filePath), true);
        EList contents = res.getContents();
        if (!(contents.get(0) instanceof CamelModel)) {
            throw new IllegalArgumentException("Root model is not of type CamelModel!");
        } else {
            return (CamelModel) contents.get(0);
        }
    }

    public static CamelModel load(File model) {
        checkNotNull(model, "model file is null");
        checkArgument(model.isFile(), "model file is not a file");
        checkArgument(model.exists(), "model file does not exist");
        return doLoad(model.getAbsolutePath());
    }

    public static CamelModel load(String model) {
        checkNotNull(model, "model string is null");
        checkArgument(!model.isEmpty(), "model string is empty");
        //write string to temporary file
        try {
            final File tempFile = File.createTempFile("camelLoader", ".camel");
            Files.write(model, tempFile, Charsets.UTF_8);
            return doLoad(tempFile.getAbsolutePath());
        } catch (IOException e) {
            throw new IllegalStateException("Can not create temporary file.");
        }
    }
}

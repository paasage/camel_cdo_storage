package org.ow2.paasage.mddb.cdo;

import eu.paasage.camel.CamelModel;
import eu.paasage.mddb.cdo.client.CDOClient;

/**
 * Created by daniel on 13.06.16.
 */
public class ModelStorage {

    private final CDOClient client;

    private ModelStorage(String cdoUser, String cdoPassword) {
        client = new CDOClient(cdoUser, cdoPassword);
    }

    public static ModelStorage of(String cdoUser, String cdoPassword) {
        return new ModelStorage(cdoUser, cdoPassword);
    }

    public void storeToCdo(CamelModel camelModel, String cdoResourceName) {
        client.storeModel(camelModel, cdoResourceName, false);
    }

}

package org.ow2.paasage.mddb.cdo;

import eu.paasage.camel.CamelPackage;
import eu.paasage.camel.deployment.DeploymentPackage;
import eu.paasage.camel.execution.ExecutionPackage;
import eu.paasage.camel.location.LocationPackage;
import eu.paasage.camel.metric.MetricPackage;
import eu.paasage.camel.organisation.OrganisationPackage;
import eu.paasage.camel.provider.ProviderPackage;
import eu.paasage.camel.requirement.RequirementPackage;
import eu.paasage.camel.scalability.ScalabilityPackage;
import eu.paasage.camel.security.SecurityPackage;
import eu.paasage.camel.type.TypePackage;
import eu.paasage.camel.unit.UnitPackage;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EPackage.Registry;

/**
 * Created by Frank on 18.11.2015.
 */
public class PackageRegistration {

    private static EPackage[] camelPackages = {
            CamelPackage.eINSTANCE,
            ScalabilityPackage.eINSTANCE,
            DeploymentPackage.eINSTANCE,
            OrganisationPackage.eINSTANCE,
            ProviderPackage.eINSTANCE,
            SecurityPackage.eINSTANCE,
            ExecutionPackage.eINSTANCE,
            TypePackage.eINSTANCE,
            RequirementPackage.eINSTANCE,
            MetricPackage.eINSTANCE,
            UnitPackage.eINSTANCE,
            LocationPackage.eINSTANCE
    };

    public static void registerCamel(Registry pr){
        for(EPackage ep : camelPackages){
            pr.put(ep.getNsURI(), ep);
        }
    }
}

